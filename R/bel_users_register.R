#' Register a new user.
#'
#' @param email String with email address.
#' @param password String with password.
#' @return API response content invisibly.
#' @export
#' @import futile.logger
#'
bel_users_register <- function(email, password) {

  resp <- backendless_api("users/register",
                  body = list(email = email,
                              password = password))

  flog.trace("User registered")

  invisible(resp$content)

}

#' Performs user log in.
#'
#' @param login Email of user.
#' @param password Password of user.
#' @param props Optional list of additional named properties to save in user profile.
#' @return `bel_session` object.
#' @seealso \url{https://backendless.com/docs/rest/doc.html#users_login}
#' @export
#' @import futile.logger
#'
bel_users_login <- function(login = NULL,
                            password = NULL,
                            props = NULL) {

  stopifnot(!is.null(login))
  stopifnot(!is.null(password))
  stopifnot(length(login) == 1L)
  stopifnot(length(password) == 1L)

  if (!is.null(props)) {
    stopifnot(is.list(props))
    stopifnot(!is.null(names(props)))
  }

  resp <- backendless_api("users/login",
                  body = c(list(login = login,
                              password = password),
                           props))

  flog.trace("User logged in")

  bel_session(bel_user_list2df(resp$content))

}
#' Performs log out of a user.
#'
#' @param session Object of class bel_session.
#'
#' @return Modified (empty) object of class bel_session.
#'
#' @seealso \url{https://backendless.com/docs/rest/doc.html#users_logout}
#' @import httr
#' @import futile.logger
#' @export

bel_users_logout <- function(session) {

  stopifnot(inherits(session, "bel_session"))
  stopifnot(loggedin(session))

  token <- token(session)

  resp <- backendless_api("users/logout",
                          method = "get",
                          add_headers(`user-token` = token))

  flog.trace("User logged out")

  bel_session()

  }
