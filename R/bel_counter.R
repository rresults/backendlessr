#' Get current value of a BEL counter.
#'
#' @param name Name of the counter.
#' @param user_token NULL by default.
#' @return Current value of the counter. Numeric.
#' @seealso \url{https://backendless.com/docs/rest/doc.html#ut_get_current}
#' @export

bel_counters_current <- function(
  name = NULL,
  user_token = NULL) {

  stopifnot(!is.null(name))

  path <- paste0(c("counters", name), collapse = "/")

    r <- backendless_api(
      path,
      method = "get",
      httr::add_headers(`user-token` = user_token))

      as.numeric(r$content)

}

#' Increment counter by 1 and return current (updated) value
#'
#' @inheritParams bel_counters_current
#' @seealso \url{https://backendless.com/docs/rest/doc.html#ut_increment_by_1__return_current}
#' @export

bel_counters_incr1get <- function(name = NULL, user_token = NULL) {

  stopifnot(!is.null(name))

  path <- paste0(c("counters", name, "increment/get"), collapse = "/")

    r <- backendless_api(
      path,
      method = "put",
      httr::add_headers(`user-token` = user_token))

      as.numeric(r$content)

}

#' Decrement counter by 1 and return current (updated) value
#'
#' @inheritParams bel_counters_current
#' @seealso \url{https://backendless.com/docs/rest/doc.html#ut_decrement_by_1__return_current}
#' @export

bel_counters_decr1get <- function(name = NULL, user_token = NULL) {

  stopifnot(!is.null(name))

  path <- paste0(c("counters", name, "decrement/get"), collapse = "/")

    r <- backendless_api(
      path,
      method = "put",
      httr::add_headers(`user-token` = user_token))

      as.numeric(r$content)

}

#' Increment counter by N and return current (updated) value
#'
#' @inheritParams bel_counters_current
#' @param value Number to add to the current counter value.
#' @seealso \url{https://backendless.com/docs/rest/doc.html#ut_increment_by_n__return_current}
#' @export

bel_counters_incrget <- function(name = NULL, value = NULL, user_token = NULL) {

  stopifnot(!is.null(name))
  stopifnot(!is.null(value))

  path <- paste0(c("counters", name, "incrementby/get"), collapse = "/")

    r <- backendless_api(
      path,
      method = "put",
      query = list(value = value),
      httr::add_headers(`user-token` = user_token)
      )

      as.numeric(r$content)

}
