## Intro

`backendlessr` is an R package to maintain users base (registration, login, logout, user profiles) and other operations (counters, data tables storage) through [Backendless](https://backendless.com/) platform. Backendless Cloud services are free of charge for up to 60 calls per minute.

Backendless is useful if you need to manage some service data but don't want to maintain your own data base for it.

## Installation

```r
remotes::install_git("https://gitlab.com/rresults/backendlessr.git")
```

## Set up

`backendlessr` package communicates with Backendless platform through [REST API](https://backendless.com/docs/rest/doc.html). You need to provide Backendless app ID and REST API key. If you run R inside of a docker (file `/.dockerenv` exists), the package takes the credentials from [Docker Swarm secrets](https://docs.docker.com/engine/swarm/secrets/) `/run/secrets/backendless_appid` and `/run/secrets/backendless_key`. Without docker container the credentials are expected to be located in system variables `BACKENDLESS_APPID` and `BACKENDLESS_KEY`.

## Use

### User registration 

Register a user with `bel_users_register()`. 

```r
user <- "testuser@rresults.consulting"
pass <- "sTr0nKFus"
if(!bel_email_used(user)) {
  bel_users_register(user, pass)
}
```

With default Backendless settings after this action a message will be sent to the user's email. The template can be change in developer's Backendless account.

### User login

Login a user with `bel_users_login()` and convert API response to a `bel_session` object. On first login Backendless platform will send a welcoming email to the user. It can be changed or switched off in Backendless settings.

```r
library(backendlessr)
usr_sssn <- bel_users_login(user, pass)
usr_sssn
```
```
Backendless user session
List of 17
 $ lastLogin        : POSIXct[1:1], format: "2018-05-30 11:42:24"
 $ userStatus       : chr "ENABLED"
 $ created          : POSIXct[1:1], format: "2018-05-30 11:33:41"
 $ ownerId          : chr "611F4CE8-4F6F-154D-FF2D-xxxxxxxxxx"
 $ socialAccount    : chr "BACKENDLESS"
 $ name             : chr NA
 $ updated          : POSIXct[1:1], format: NA
 $ objectId         : chr "611F4CE8-4F6F-154D-FF2D-xxxxxxxxxx"
 $ email            : chr "testuser@rresults.consulting"
 $ class            : chr "Users"
 $ user_token       : chr "8EF278A6-25C9-DE12-FF99-E0E91D843900"
 - attr(*, "class")= chr "bel_session"
```

Is the user session active? Function `loggedin()` currently checks that the `bel_session` object is not empty. It doesn't ask the API, so a developer must update session manually after logout.

```r
loggedin(usr_sssn)
```
```
[1] TRUE
```

```r
token(usr_sssn)
```
```
[1] "8EF278A6-25C9-DE12-FF99-E0E91D843900"
```

### Additional data in user profile

```r
datum <- as.character(Sys.time())
usr_sssn <- bel_users_update(usr_sssn, testel = datum)
```

```
 Error: Backendless API request failed [400]
Unable to save object - invalid data type for properties - testel. You can change the property type in developer console.
<1007> 
```

Oops... Field named `testel` has type logical.

```r
usr_sssn <- bel_users_update(usr_sssn, testel = FALSE)
usr_sssn$testel
```

```
[1] FALSE
```

New elements are also allowed and the table scheme will be updated automatically:

```r
usr_sssn <- bel_users_update(usr_sssn, testel1 = datum)
usr_sssn$testel1
```
```
[1] "2018-05-30 12:20:09"
```
### User logout

```r
usr_sssn <- bel_users_logout(usr_sssn)
loggedin(usr_sssn)
```
```
FALSE
```

### User removal

In case you want to implement proper GDRP policy you would like to remove user records :) 

```r
bel_users_delete(user)
```

## Other functions supported by package

### General and user-specific counters


Current value of `test_counter`: 

```r
bel_counters_current("test_counter")
```
```
217
```


Increment value by one and get the result:

```r
bel_counters_incr1get("test_counter")
```

```
218
```

Increment value by `n` and get the result:

```r
n <- 10
bel_counters_incrget("test_counter", n)
```
```
228
```

Decrement counter by one and get the result:
```r
bel_counters_decr1get("test_counter")
```
```
227
```

### Save and retrieve data

Use `bel_data_save(table_name, data)` to write data to a table in Backendless. Currently data must be in JSON format. Use `bel_data_where()` to retrieve stored data.

## Backendless platform features not supported by package

There are more features provided by Backendless platform, and notable are the following:

* Social logins (e.g. to allow a user to use their Facebook account to log in to our service);
* Async calls;
* Geolocation;
* Logging (send log messages of your app to Backendless platform).


## Support

Please fill an [issue](https://gitlab.com/rresults/backendlessr/issues/new) in case of questions or problems.


## Backendless plans and discounts

Backendless offers a free plan (60 requests per minute). If you need more than that, you can choose a paid plan. Use our invite code `p6tvk3` when you create a new app to get 25% off for the first 6 months. We'll get $10 credit after your signup.

