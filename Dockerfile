FROM rocker/r-base:latest

MAINTAINER Alexander Matrunich "a@rresults.consulting"

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	    libcurl4-openssl-dev \
	    libssl-dev \
	&& rm -rf /var/lib/apt/lists/*

RUN R -q -e "\
    install.packages(c('remotes', \ 
    'dplyr', 'futile.logger', \
    'httr', \
    'jsonlite', \
    'tibble', \
    'purrr', \
    'shiny', \
    'shinyjs', \
    'stringr' \
    ), \
    repos = 'https://cloud.r-project.org')" \
    && rm -rf /tmp/downloaded_packages/ /tmp/*.rds 


RUN R -q -e "\
    remotes::install_gitlab(c( \
    'rresults/backendlessr'), \
    dependencies = TRUE, \
    repos = 'https://cloud.r-project.org')" \
    && rm -rf /tmp/downloaded_packages/ /tmp/*.rds 

EXPOSE 3838

CMD R -q -e "backendlessr::shiny_demo_login()"
