% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/module_user_register.R
\name{moduleRegisterUserUI}
\alias{moduleRegisterUserUI}
\title{Shiny UI module for user registration}
\usage{
moduleRegisterUserUI(id = NULL, htmlid = "registration-form")
}
\arguments{
\item{id}{Namespace for the module. String.}

\item{htmlid}{Id for HTML tag div. By default "registration-form".}
}
\description{
Shiny UI module for user registration
}
